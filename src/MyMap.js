import {  GoogleMap, useLoadScript, Marker } from "@react-google-maps/api";
import { useMemo, useEffect, useState } from "react";
import { getCityData } from "./data/CityData.js";
import './MyMap.css';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { getDistanceFromLatLonInKm } from "./data/CityData.js";
import Slider from '@mui/material/Slider';
import Grid from '@mui/material/Grid';

export default function MyMap(){
    //console.log("@MyMap gmap api key:%o", process.env.REACT_APP_GOOGLE_MAP_API_KEY);
    const {isLoaded} = useLoadScript({
        googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAP_API_KEY
    })

    if (!isLoaded) return <div>Loading Map...</div>
    return <MyMapInner/>
}

function MyMapInner(){
    const mapCenter=useMemo(()=>({lat: 49.8844, lng: -97.1464}),[]);
    const [province, setProvince]=useState('');
    const [provinces, setProvinces]=useState([]);
    const [city, setCity]=useState('');
    const [cities, setCities]=useState([]);
    const cityData = useMemo(()=>{return getCityData()},[]);
    const [minDistanceRange, setMinDistanceRange]=useState(0);
    const [maxDistanceRange, setMaxDistanceRange]=useState(0);
    const [distanceRange, setDistanceRange]=useState([0,0]);
    const [cityDistances, setCityDistances]=useState([]);
    useEffect(()=>{
        console.log("In useEffect");

        const newMarkers=[];
        const newProvinces=[];
        cityData.forEach( city=>{
            newMarkers.push({...city});
            newProvinces[city.province_id]=city.province_name;
        });

        const p = [];
        for(const [k, v] of Object.entries(newProvinces)){
            p.push({id: k, name: v});
        }
        p.sort((q, r)=>( q["name"] > r["name"] ? 1 : -1 ));
        setProvinces(prevProvinces=>{ return p;});

    },[cityData])


    /** TO DO */
    //const bounds=new window.google.maps.LatLngBounds(...markers);

    return <><GoogleMap zoom={4} center={mapCenter} mapContainerClassName="map-container">
        {
        cityDistances.filter((d)=>{
            if (distanceRange[0]<=d.distanceInKm && d.distanceInKm<=distanceRange[1]){
                return true;
            }
            return false;
        })
        .map( (marker, idx)=>{
            return <Marker
                key={idx}
                position={{lat:marker.lat, lng:marker.lng}}
                onClick={ ()=>{
                    console.log("you clicked on marker# %o: %o", idx, marker.city_ascii);
               }}
                tooltip={marker.city_ascii}
                text={idx}
                title={marker.city_ascii}
                // onMouseOver={()=>{
                //     console.log("onMouseOver: %o", marker.city_ascii);
                // }}
                // onMouseOut={()=>{
                //     console.log("onMouseOut: %o", marker.city_ascii);
                // }}
            />
            }
        )}
        </GoogleMap>
        <div>
            <FormControl sx={{ m: 1, minWidth: 200 }}>
                <InputLabel id="select-province-label">Province</InputLabel>
                <Select
                    labelId="select-province-label"
                    id="select-province"
                    value={province}
                    label="ProvinceLabel"
                    onChange={(e)=>{
                        console.log("onChange: %o", e.target.value );
                        setCity('');
                        setProvince(e.target.value);
                        const cities=cityData.filter( (c)=>{ return c.province_id===e.target.value})
                            .map( c=>{
                                return {
                                    id: c.id,
                                    name: c.city_ascii
                                };
                            })
                            .sort( (c,d)=>( (c.name>d.name)? 1 : -1 ));
                        setCities(cities);
                    }}
                >
                    <MenuItem value=""><em>None</em></MenuItem>
                    {provinces.map( (province, idx)=>{
                        return <MenuItem key={province.id} value={province.id}>{province.name}</MenuItem>
                    })}
                </Select>
            </FormControl>

            <FormControl sx={{ m: 1, minWidth: 200 }}>
                <InputLabel id="select-city-label">City</InputLabel>
                <Select
                    labelId="select-city-label"
                    id="select-city"
                    value={city}
                    label="CityLabel"
                    onChange={(e)=>{
                        setCity(e.target.value);
                        console.log("onChange: %o", e.target.value );
                        //+++ Calculate distances +++
                        const baseCity = cityData.find( c=>c.id===e.target.value);
                        const d=cityData.map( cd=>{
                            return {
                                ...cd, distanceInKm: Math.round(getDistanceFromLatLonInKm(baseCity.lat, baseCity.lng, cd.lat, cd.lng))
                            }
                        })
                        .sort( (e,f)=>( e.distanceInKm > f.distanceInKm ? 1 : -1));
                        //d.shift();
                        const minDistance=d[0].distanceInKm;
                        const maxDistance=d[d.length-1].distanceInKm;
                        setDistanceRange([d[0].distanceInKm, d[d.length-1].distanceInKm]);
                        setMinDistanceRange(minDistance);
                        setMaxDistanceRange(maxDistance);

                        console.log("minDistance: %o", minDistance);
                        console.log("maxDistance: %o", maxDistance);

                        console.log("distances: %o", d);
                        setCityDistances(d);

                        //--- Calculate distances ---
                    }}
                >
                    <MenuItem value=""><em>{ (province==='')?'Select a Province': 'None' }</em></MenuItem>

                    {cities.map( (city, idx)=>{
                        return <MenuItem key={idx} value={city.id}>{city.name}</MenuItem>
                    })}
                </Select>
            </FormControl>

            <Grid container spacing={2} alignItems="center">
                <Grid item sx={{ m: 1, minWidth: 120 }}>
                    {distanceRange[0]}Km
                </Grid>
                <Grid item sx={{ m: 1, minWidth: 200 }}>
                    <Slider
                        getAriaLabel={() =>{ return 'MyAiraLabel'} }
                        value={distanceRange}
                        onChange={(event, newValue)=>{
                            setDistanceRange(newValue);
                        }}
                        valueLabelDisplay="on"
                        getAriaValueText={(value)=>{return value+'Km';}}
                        min={minDistanceRange}
                        max={maxDistanceRange}
                        disableSwap

                    />
                </Grid>
                <Grid item sx={{ m: 1, minWidth: 120 }}>
                    {distanceRange[1]}Km
                </Grid>
            </Grid>
        </div>


        </>
}